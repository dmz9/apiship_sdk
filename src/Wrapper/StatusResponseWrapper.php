<?php

namespace Apiship\Wrapper;

use Apiship\Entity\Response\StatusResponse;

class StatusResponseWrapper
{
	public static $errorStatusKeys = [
		'uploadingError',
		'problem',
		'notApplicable',
		'unknown'
	];
	/**
	 * @var StatusResponse
	 */
	protected $statusResponse;

	public function __construct(StatusResponse $statusResponse)
	{
		$this->statusResponse = $statusResponse;
	}

	public function getStatus()
	{
		return $this->statusResponse->getStatus();
	}

	public function getOrderInfo()
	{
		return $this->statusResponse->getOrderInfo();
	}
	
	/**
	 * @return bool
	 */
	public function isSuccessful()
	{
		return !in_array(
			$this->statusResponse->getStatus()
			                     ->getKey(),
			self::$errorStatusKeys
		);
	}
}