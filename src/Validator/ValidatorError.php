<?php

namespace Apiship\Validator;
class ValidatorError
{
	protected $message;
	protected $fieldName;
	protected $objectName;
	
	public function __construct($message, $fieldName = null, $objectName = null)
	{
		$this->message    = (string)$message;
		$this->fieldName  = (!empty($fieldName))
			? (string)$fieldName
			: null;
		$this->objectName = (!empty($objectName))
			? (string)$objectName
			: null;
	}
	
	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @return null|string
	 */
	public function getFieldName()
	{
		return $this->fieldName;
	}
	
	/**
	 * @return null|string
	 */
	public function getObjectName()
	{
		return $this->objectName;
	}
}