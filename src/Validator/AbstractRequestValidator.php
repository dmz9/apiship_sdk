<?php

namespace Apiship\Validator;

use Apiship\Entity\AbstractRequest;

abstract class AbstractRequestValidator
{
	protected $errorPrototype = null;
	protected $errors = [];
	protected $simpleMode = true;

	public function __construct(ValidatorError $errorPrototype = null)
	{
		if ($errorPrototype !== null) {
			$this->errorPrototype = get_class($errorPrototype);
			$this->simpleMode     = false;
		}
	}

	abstract function validate(AbstractRequest $request);

	public function getErrors()
	{
		return $this->errors;
	}

	protected function error($message, $field = null, $object = null)
	{
		$this->errors[] = $this->simpleMode
			? (string)$message
			: new $this->errorPrototype(
				$message,
				$field,
				$object
			);
	}
}