<?php

namespace Apiship\Validator;

use Apiship\Entity\AbstractRequest;
use Apiship\Entity\Request\OrderRequest;

class OrderRequestValidator extends AbstractRequestValidator
{
	const PICKUP_TYPE__COURIER = 1;
	const PICKUP_TYPE__SELF = 2;
	const DELIVERY_TYPE__COURIER = 1;
	const DELIVERY_TYPE__SELF = 2;
	/**
	 * @param AbstractRequest $orderRequest
	 *
	 * @return string[]|ValidatorError[]
	 */
	public function validate(AbstractRequest $orderRequest)
	{
		if($orderRequest instanceof OrderRequest){
			$this->_validateRequiredFields($orderRequest);
			$this->_validateVariants($orderRequest);
		}else{
			$this->error("Validated object must be instance of OrderRequest!");
		}
		return $this->getErrors();
	}

	/**
	 * валидация вариантов.
	 * некоторые свойства могут быть взаимоисключающими, а некоторые могут требовать определенных значений или наличия других связаных свойств
	 * @param OrderRequest $orderRequest
	 */
	private function _validateVariants(OrderRequest $orderRequest)
	{
		$order = $orderRequest->getOrder();

		if ($order->getPickupType() == self::PICKUP_TYPE__SELF) {
			if ((int)$order->getPointInId() <= 0) {
				$this->error("Для типа забора 'Доставка на пункт приема' указана некорректная точка забора {$order->getPointInId()} заказа");
			}
		}

		if ($order->getDeliveryType() == self::DELIVERY_TYPE__SELF) {
			if ((int)$order->getPointOutId() <= 0) {
				$errors[] = "Для типа доставки 'Самовывоз' указана некорректная точка самовывоза {$order->getPointOutId()}";
			}
		}
	}
	private function _validateRequiredFields(OrderRequest $orderRequest)
	{
		//todo доделать валидацию
		$order = $orderRequest->getOrder();
		if (empty($order)) {
			$this->error("Empty OrderData");
		} else {
			!empty($order->getClientNumber())
				?: $this->error('empty clientNumber');
			
			!empty($order->getWeight())
				?: $this->error('empty weight');
			
			!empty($order->getProviderKey())
				?: $this->error('empty provider key');
			
			!empty($order->getPickupType())
				?: $this->error('empty pickupType');
			
			!empty($order->getDeliveryType())
				?: $this->error('empty deliveryType');
			
			!empty($order->getTariffId())
				?: $this->error('empty tariff id');
		}
		
		$cost = $orderRequest->getCost();
		if (empty($cost)) {
			$this->error("Empty Cost");
		} else {
			null !== $cost->getAssessedCost()
				?: $this->error("empty assessed cost");
			null !== $cost->getCodCost()
				?: $this->error("empty cod cost");
		}
		
		$sender = $orderRequest->getSender();
		if (empty($sender)) {
			$this->error("Empty Sender");
		} else {
			!empty($sender->getCountryCode())
				?: $this->error('empty country code');
			
			!empty($sender->getRegion())
				?: $this->error('empty region');
			
			!empty($sender->getCity())
				?: $this->error('empty city');

			!empty($sender->getStreet())
				?: $this->error('empty street');
			
			!empty($sender->getHouse())
				?: $this->error('empty house');
			
			!empty($sender->getContactName())
				?: $this->error('empty contact name');
			
			!empty($sender->getPhone())
				?: $this->error('empty phone');
		}
		
		$recipient = $orderRequest->getRecipient();
		if (empty($recipient)) {
			$this->error("Empty Recipient");
		} else {
			!empty($recipient->getCountryCode())
				?: $this->error('empty country code');
			
			!empty($recipient->getRegion())
				?: $this->error('empty region');
			
			!empty($recipient->getCity())
				?: $this->error('empty city');
			
			!empty($recipient->getStreet())
				?: $this->error('empty street');
			
			!empty($recipient->getHouse())
				?: $this->error('empty house');
			
			!empty($recipient->getContactName())
				?: $this->error('empty contact name');
			
			!empty($recipient->getPhone())
				?: $this->error('empty phone');
		}
		
	}
}