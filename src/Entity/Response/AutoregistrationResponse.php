<?php

namespace Apiship\Entity\Response;

use Apiship\Entity\AbstractResponse;

class AutoregistrationResponse extends AbstractResponse
{
	/**
	 * @var string
	 */
	protected $login;
	/**
	 * @var string
	 */
	protected $password;
	
	/**
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}
	
	/**
	 * @param string $login
	 *
	 * @return AutoregistrationResponse
	 */
	public function setLogin($login)
	{
		$this->login = $login;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 *
	 * @return AutoregistrationResponse
	 */
	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}
}