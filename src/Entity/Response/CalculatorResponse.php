<?php

namespace Apiship\Entity\Response;

use Apiship\Entity\AbstractResponse;
use Apiship\Entity\Response\Part\CalculatorResult;
use Apiship\Entity\Response\Part\CalculatorToDoorResult;
use Apiship\Entity\Response\Part\CalculatorToPointResult;

class CalculatorResponse extends AbstractResponse
{
    /**
     * @var CalculatorToDoorResult[] объект доставки до двери
     */
    protected $deliveryToDoor = [];
    /**
     * @var CalculatorToPointResult[] объект доставки до точки
     */
    protected $deliveryToPoint = [];

    /**
     * @return CalculatorToDoorResult[]|null
     */
    public function getDeliveryToDoor()
    {
        return $this->deliveryToDoor;
    }

    /**
     * @param CalculatorToDoorResult[] $deliveryToDoor
     *
     * @return $this
     */
    public function setDeliveryToDoor(array $deliveryToDoor)
    {
        foreach ($deliveryToDoor as $item) {
            $this->addDeliveryToDoor($item);
        }

        return $this;
    }

    /**
     * @param CalculatorToDoorResult $deliveryToDoor
     *
     * @return CalculatorResponse
     */
    public function addDeliveryToDoor(CalculatorToDoorResult $deliveryToDoor)
    {
        $this->deliveryToDoor[] = $deliveryToDoor;

        return $this;
    }

    /**
     * @return CalculatorToPointResult[]|null
     */
    public function getDeliveryToPoint()
    {
        return $this->deliveryToPoint;
    }

    /**
     * @param CalculatorToPointResult[]|null $deliveryToPoint
     *
     * @return $this
     */
    public function setDeliveryToPoint($deliveryToPoint)
    {
        foreach ($deliveryToPoint as $item) {
            $this->addDeliveryToPoint($item);
        }
        return $this;
    }

    /**
     * @param CalculatorToPointResult $deliveryToPoint
     *
     * @return $this
     */
    public function addDeliveryToPoint(CalculatorToPointResult $deliveryToPoint)
    {
        $this->deliveryToPoint[] = $deliveryToPoint;

        return $this;
    }
}