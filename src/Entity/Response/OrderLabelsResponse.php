<?php

namespace Apiship\Entity\Response;

use Apiship\Entity\AbstractResponse;
use Apiship\Entity\Response\Part\ErrorOrderInfo;

class OrderLabelsResponse extends AbstractResponse
{
	/**
	 * url файла с наклейками <br/>
	 * для одиночного файла наклеек это будет ссылка на pdf <br/>
	 * для нескольких наклеек отдается ссылка на zip-архив <br/>
	 * наклейки создаются только для успешно загруженых заказов, остальные заказы для которых невозможно создать наклейки указываются в {@link $failedOrders}
	 * @var string
	 */
	protected $url;
	/**
	 * массив с идентификаторами заказа для которых невозможно создать наклейки
	 * @var ErrorOrderInfo[]
	 */
	protected $failedOrders;

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return OrderLabelsResponse
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return ErrorOrderInfo[]
	 */
	public function getFailedOrders()
	{
		return $this->failedOrders;
	}

	/**
	 * @param ErrorOrderInfo[] $failedOrders
	 * @return OrderLabelsResponse
	 */
	public function setFailedOrders($failedOrders)
	{
		$this->failedOrders = $failedOrders;
		return $this;
	}
}