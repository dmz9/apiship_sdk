<?php

namespace Apiship\Entity\Response\Part;

use Apiship\Entity\AbstractResponsePart;
use Apiship\Entity\AsArrayBehavior;
use Apiship\Entity\MagicMethodsBehavior;

abstract class Tariff extends AbstractResponsePart
{
	use MagicMethodsBehavior;
	use AsArrayBehavior;
	/**
	 * @var string id тарифа в службе доставки
	 */
	protected $tariffProviderId;
	/**
	 * @var int ID тарифа
	 */
	protected $tariffId;
	/**
	 * @var string Наименование тарифа
	 */
	protected $tariffName;
	/**
	 * @var int[] Типы забора (см. /lists/pickupTypes), если не переданы берутся оба типа
	 */
	protected $pickupTypes;
	/**
	 * @var int[] Типы доставки (см. /lists/deliveryTypes), если не переданы берутся оба типа
	 */
	protected $deliveryTypes;
	/**
	 * @var float Стоимость доставки
	 */
	protected $deliveryCost;
	/**
	 * @var int Минимальное количество дней на доставку
	 */
	protected $daysMin;
	/**
	 * @var int Максимальное количество дней на доставку
	 */
	protected $daysMax;
	/**
	 * @deprecated
	 * @var string Откуда производится доставка
	 * (door - от двери клиента, point - от терминала службы доставки, pointOrDoor - любым из предыдущих способом
	 */
	protected $from;
	
	/**
	 * @return string
	 */
	public function getTariffProviderId()
	{
		return $this->tariffProviderId;
	}
	
	/**
	 * @param string $tariffProviderId
	 *
	 * @return $this
	 */
	public function setTariffProviderId($tariffProviderId)
	{
		$this->tariffProviderId = $tariffProviderId;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getTariffId()
	{
		return $this->tariffId;
	}
	
	/**
	 * @param int $tariffId
	 *
	 * @return $this
	 */
	public function setTariffId($tariffId)
	{
		$this->tariffId = $tariffId;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTariffName()
	{
		return $this->tariffName;
	}
	
	/**
	 * @param string $tariffName
	 *
	 * @return $this
	 */
	public function setTariffName($tariffName)
	{
		$this->tariffName = $tariffName;
		return $this;
	}
	
	/**
	 * @return \int[]
	 */
	public function getPickupTypes()
	{
		return $this->pickupTypes;
	}
	
	/**
	 * @param \int[] $pickupTypes
	 *
	 * @return $this
	 */
	public function setPickupTypes($pickupTypes)
	{
		$this->pickupTypes = $pickupTypes;
		return $this;
	}
	
	/**
	 * @return \int[]
	 */
	public function getDeliveryTypes()
	{
		return $this->deliveryTypes;
	}
	
	/**
	 * @param \int[] $deliveryTypes
	 *
	 * @return $this
	 */
	public function setDeliveryTypes($deliveryTypes)
	{
		$this->deliveryTypes = $deliveryTypes;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getDeliveryCost()
	{
		return $this->deliveryCost;
	}
	
	/**
	 * @param float $deliveryCost
	 *
	 * @return $this
	 */
	public function setDeliveryCost($deliveryCost)
	{
		$this->deliveryCost = $deliveryCost;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getDaysMin()
	{
		return $this->daysMin;
	}
	
	/**
	 * @param int $daysMin
	 *
	 * @return $this
	 */
	public function setDaysMin($daysMin)
	{
		$this->daysMin = $daysMin;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getDaysMax()
	{
		return $this->daysMax;
	}
	
	/**
	 * @param int $daysMax
	 *
	 * @return $this
	 */
	public function setDaysMax($daysMax)
	{
		$this->daysMax = $daysMax;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFrom()
	{
		return $this->from;
	}
	
	/**
	 * @param string $from
	 *
	 * @return Tariff
	 */
	public function setFrom($from)
	{
		$this->from = $from;
		return $this;
	}
}