<?php

namespace Apiship\Entity\Response\Part;

class CalculatorToDoorResult extends CalculatorResult
{
	/**
	 * @var TariffToDoor[]
	 */
	protected $tariffs = [];
	
	/**
	 * @param TariffToDoor $tariffToDoor
	 *
	 * @return $this
	 */
	public function addTariff(TariffToDoor $tariffToDoor)
	{
		$this->tariffs[] = $tariffToDoor;
		return $this;
	}
	
	/**
	 * @return TariffToDoor[]
	 */
	public function getTariffs()
	{
		return $this->tariffs;
	}
}