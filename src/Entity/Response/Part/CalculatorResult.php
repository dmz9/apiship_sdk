<?php

namespace Apiship\Entity\Response\Part;

use Apiship\Entity\AbstractResponsePart;

abstract class CalculatorResult extends AbstractResponsePart
{
	/**
	 * @var string Ключ провайдера
	 */
	protected $providerKey;
	
	/**
	 * @return string
	 */
	public function getProviderKey()
	{
		return $this->providerKey;
	}
	
	/**
	 * @param string $providerKey
	 *
	 * @return $this
	 */
	public function setProviderKey($providerKey)
	{
		$this->providerKey = $providerKey;
		return $this;
	}

}