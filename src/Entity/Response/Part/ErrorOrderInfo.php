<?php

namespace Apiship\Entity\Response\Part;

use Apiship\Entity\AbstractResponsePart;

/**
 * часть ответа сервера из метода /orders/labels
 *
 * Class ErrorOrderInfo
 * @package Apiship\Entity\Response\Part
 */
class ErrorOrderInfo extends AbstractResponsePart
{
	/**
	 * идентификатор заказа для которого невозможно было создать наклейку
	 * @var int
	 */
	protected $orderId;
	/**
	 * Описание причины неудачного получения статуса по заказу
	 * @var string
	 */
	protected $message;

	/**
	 * @return int
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param int $orderId
	 * @return ErrorOrderInfo
	 */
	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 * @return ErrorOrderInfo
	 */
	public function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}
}