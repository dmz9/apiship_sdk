<?php

namespace Apiship\Entity\Response\Part;

class CalculatorToPointResult extends CalculatorResult
{
	/**
	 * @var TariffToPoint[]
	 */
	protected $tariffs = [];
	
	/**
	 * @param TariffToPoint $tariffToPoint
	 *
	 * @return $this
	 */
	public function addTariff(TariffToPoint $tariffToPoint)
	{
		$this->tariffs[] = $tariffToPoint;
		return $this;
	}
	
	/**
	 * @return TariffToPoint[]
	 */
	public function getTariffs()
	{
		return $this->tariffs;
	}
}