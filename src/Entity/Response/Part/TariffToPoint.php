<?php

namespace Apiship\Entity\Response\Part;
class TariffToPoint extends Tariff
{
	/**
	 * @var int[] Массив идентификаторов доступных ПВЗ для каждого из тарифов
	 */
	protected $pointIds;
	
	/**
	 * @return \int[]
	 */
	public function getPointIds()
	{
		return $this->pointIds;
	}
	
	/**
	 * @param \int[] $pointIds
	 *
	 * @return $this
	 */
	public function setPointIds($pointIds)
	{
		$this->pointIds = $pointIds;
		return $this;
	}
}