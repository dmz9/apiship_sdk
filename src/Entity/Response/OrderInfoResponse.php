<?php

namespace Apiship\Entity\Response;

use Apiship\Entity\AbstractResponse;
use Apiship\Entity\AsArrayBehavior;
use Apiship\Entity\MagicMethodsBehavior;
use Apiship\Entity\Response\Part\Cost;
use Apiship\Entity\Response\Part\ExtraParam;
use Apiship\Entity\Response\Part\Item;
use Apiship\Entity\Response\Part\OrderInfoData;
use Apiship\Entity\Response\Part\Place;
use Apiship\Entity\Response\Part\Recipient;
use Apiship\Entity\Response\Part\Sender;
use Apiship\Exception\RequiredParameterException;

class OrderInfoResponse extends AbstractResponse
{
	use MagicMethodsBehavior;
	use AsArrayBehavior;
	/**
	 * @var OrderInfoData Информация о заказе
	 */
	protected $orderInfoData;
	/**
	 * @var Cost Информация о цене
	 */
	protected $cost;
	/**
	 * @var Sender Инфрмация об отправителе
	 */
	protected $sender;
	/**
	 * @var Recipient Информация о получателе
	 */
	protected $recipient;
	/**
	 * @var Item[] Массив с вложениями
	 */
	protected $items;
	/**
	 * @var Place[] Массив с местами для перевозки
	 */
	protected $places;
	/**
	 * @var ExtraParam[] Массив доп параметрами
	 */
	protected $extraParams;

	/**
	 * @return OrderInfoData
	 * @throws RequiredParameterException
	 */
	public function getOrder()
	{
		if (!$this->orderInfoData) {
			throw new RequiredParameterException(
				'Property "' . get_class($this) . '::order" is required.
                ');
		}

		return $this->orderInfoData;
	}

	/**
	 * @param OrderInfoData $order
	 *
	 * @return OrderInfoResponse
	 */
	public function setOrder(OrderInfoData $order)
	{
		$this->orderInfoData = $order;
		return $this;
	}

	/**
	 * @return Cost
	 * @throws RequiredParameterException
	 */
	public function getCost()
	{
		if (!$this->cost) {
			throw new RequiredParameterException(
				'Property "' . get_class($this) . '::cost" is required.
                ');
		}

		return $this->cost;
	}

	/**
	 * @param Cost $cost
	 *
	 * @return OrderInfoResponse
	 */
	public function setCost(Cost $cost)
	{
		$this->cost = $cost;
		return $this;
	}

	/**
	 * @return Sender
	 * @throws RequiredParameterException
	 */
	public function getSender()
	{
		if (!$this->sender) {
			throw new RequiredParameterException(
				'Property "' . get_class($this) . '::sender" is required.
                ');
		}

		return $this->sender;
	}

	/**
	 * @param Sender $sender
	 *
	 * @return OrderInfoResponse
	 */
	public function setSender(Sender $sender)
	{
		$this->sender = $sender;
		return $this;
	}

	/**
	 * @return Recipient
	 * @throws RequiredParameterException
	 */
	public function getRecipient()
	{
		if (!$this->recipient) {
			throw new RequiredParameterException(
				'Property "' . get_class($this) . '::recipient" is required.
                ');
		}

		return $this->recipient;
	}

	/**
	 * @param Recipient $recipient
	 *
	 * @return OrderInfoResponse
	 */
	public function setRecipient(Recipient $recipient)
	{
		$this->recipient = $recipient;
		return $this;
	}

	/**
	 * @return Item[]
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param Item[] $items
	 *
	 * @return OrderInfoResponse
	 */
	public function setItems(array $items)
	{
		foreach ($items as $item) {
			$this->addItem($item);
		}

		return $this;
	}

	/**
	 * @param Item $item
	 *
	 * @return OrderInfoResponse
	 */
	public function addItem(Item $item)
	{
		$this->items[] = $item;

		return $this;
	}

	/**
	 * @return Place[]
	 */
	public function getPlaces()
	{
		return $this->places;
	}

	/**
	 * @param Place[] $places
	 *
	 * @return OrderInfoResponse
	 */
	public function setPlaces(array $places)
	{
		foreach ($places as $place) {
			$this->addPlace($place);
		}

		return $this;
	}

	/**
	 * @param Place $place
	 *
	 * @return OrderInfoResponse
	 */
	public function addPlace(Place $place)
	{
		$this->places[] = $place;

		return $this;
	}

	/**
	 * @return ExtraParam[]
	 */
	public function getExtraParams()
	{
		return $this->extraParams;
	}

	/**
	 * @param ExtraParam[] $extraParams
	 *
	 * @return OrderInfoResponse
	 */
	public function setExtraParams(array $extraParams)
	{
		foreach ($extraParams as $extraParam) {
			$this->addExtraParam($extraParam);
		}

		return $this;
	}

	/**
	 * @param ExtraParam $extraParam
	 *
	 * @return OrderInfoResponse
	 */
	public function addExtraParam(ExtraParam $extraParam)
	{
		$this->extraParams[] = $extraParam;

		return $this;
	}
}