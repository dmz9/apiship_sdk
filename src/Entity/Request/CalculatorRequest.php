<?php

namespace Apiship\Entity\Request;

use Apiship\Entity\AbstractRequest;
use Apiship\Entity\Request\Part\From;
use Apiship\Entity\Request\Part\To;

class CalculatorRequest extends AbstractRequest
{
	/**
	 * @var \Apiship\Entity\Request\Part\From Информация о пункте отправления
	 */
	protected $from;
	/**
	 * @var To Информация о пункте получения
	 */
	protected $to;
	/**
	 * @var integer Вес всего заказа (в граммах)
	 */
	protected $weight;
	/**
	 * @var integer Ширина заказа (в сантиметрах)
	 */
	protected $width;
	/**
	 * @var integer Высота заказа (в сантиметрах)
	 */
	protected $height;
	/**
	 * @var integer Длина заказа (в сантиметрах)
	 */
	protected $length;
	/**
	 * @var float Оценочная стоимость (в рублях). по умолчанию = 0
	 */
	protected $assessedCost;
	/**
	 * @var string Дата приёма груза (не обязательно, по умолчания берется текущая дата)
	 */
	protected $pickupDate;
	/**
	 * @var int[] Типы забора (см. /lists/pickupTypes), если не переданы берутся оба типа
	 */
	protected $pickupTypes;
	/**
	 * @var int[] Типы доставки (см. /lists/deliveryTypes), если не переданы берутся оба типа
	 */
	protected $deliveryTypes;
	/**
	 * @var float Сумма наложенного платежа
	 */
	protected $codCost;
	/**
	 * @var array Массив ключей СД, для которых вызывать метод калькуляции
	 */
	protected $providerKeys;
	/**
	 * @var integer Время ожидания ответа от провайдера, результаты по провайдерам,
	 * которые не успели в указанное время, выдаваться не будут. Если не указывать, будет ожидаться ответ от всех.
	 */
	protected $timeout;
	
	/**
	 * @return From
	 */
	public function getFrom()
	{
		return $this->from;
	}
	
	/**
	 * @param From $from
	 *
	 * @return CalculatorRequest
	 */
	public function setFrom($from)
	{
		$this->from = $from;
		return $this;
	}
	
	/**
	 * @return To
	 */
	public function getTo()
	{
		return $this->to;
	}
	
	/**
	 * @param To $to
	 *
	 * @return CalculatorRequest
	 */
	public function setTo($to)
	{
		$this->to = $to;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getWeight()
	{
		return $this->weight;
	}
	
	/**
	 * @param int $weight
	 *
	 * @return CalculatorRequest
	 */
	public function setWeight($weight)
	{
		$this->weight = $weight;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getWidth()
	{
		return $this->width;
	}
	
	/**
	 * @param int $width
	 *
	 * @return CalculatorRequest
	 */
	public function setWidth($width)
	{
		$this->width = $width;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}
	
	/**
	 * @param int $height
	 *
	 * @return CalculatorRequest
	 */
	public function setHeight($height)
	{
		$this->height = $height;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getLength()
	{
		return $this->length;
	}
	
	/**
	 * @param int $length
	 *
	 * @return CalculatorRequest
	 */
	public function setLength($length)
	{
		$this->length = $length;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getAssessedCost()
	{
		return $this->assessedCost;
	}
	
	/**
	 * @param float $assessedCost
	 *
	 * @return CalculatorRequest
	 */
	public function setAssessedCost($assessedCost)
	{
		$this->assessedCost = $assessedCost;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPickupDate()
	{
		return $this->pickupDate;
	}
	
	/**
	 * @param string $pickupDate
	 *
	 * @return CalculatorRequest
	 */
	public function setPickupDate($pickupDate)
	{
		$this->pickupDate = $pickupDate;
		return $this;
	}
	
	/**
	 * @return \int[]
	 */
	public function getPickupTypes()
	{
		return $this->pickupTypes;
	}
	
	/**
	 * @param \int[] $pickupTypes
	 *
	 * @return CalculatorRequest
	 */
	public function setPickupTypes($pickupTypes)
	{
		$this->pickupTypes = $pickupTypes;
		return $this;
	}
	
	/**
	 * @return \int[]
	 */
	public function getDeliveryTypes()
	{
		return $this->deliveryTypes;
	}
	
	/**
	 * @param \int[] $deliveryTypes
	 *
	 * @return CalculatorRequest
	 */
	public function setDeliveryTypes($deliveryTypes)
	{
		$this->deliveryTypes = $deliveryTypes;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getCodCost()
	{
		return $this->codCost;
	}
	
	/**
	 * @param float $codCost
	 *
	 * @return CalculatorRequest
	 */
	public function setCodCost($codCost)
	{
		$this->codCost = $codCost;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getProviderKeys()
	{
		return $this->providerKeys;
	}

	/**
	 * @param array $providerKeys
	 *
	 * @return CalculatorRequest
	 */
	public function setProviderKeys($providerKeys)
	{
		$this->providerKeys = $providerKeys;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTimeout()
	{
		return $this->timeout;
	}

	/**
	 * @param int $timeout
	 *
	 * @return CalculatorRequest
	 */
	public function setTimeout($timeout)
	{
		$this->timeout = $timeout;
		return $this;
	}
}