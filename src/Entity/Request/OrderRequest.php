<?php

namespace Apiship\Entity\Request;

use Apiship\Entity\AbstractRequest;
use Apiship\Entity\Request\Part\OrderData;
use Apiship\Entity\Request\Part\Cost;
use Apiship\Entity\Request\Part\Sender;
use Apiship\Entity\Request\Part\Recipient;
use Apiship\Entity\Request\Part\Item;
use Apiship\Entity\Request\Part\Place;
use Apiship\Entity\Request\Part\ExtraParam;
use Apiship\Exception\RequiredParameterException;

class OrderRequest extends AbstractRequest
{
    /**
     * @var OrderData Информация о заказе
     */
    protected $order;
    /**
     * @var \Apiship\Entity\Request\Part\Cost Информация о цене
     */
    protected $cost;
    /**
     * @var \Apiship\Entity\Request\Part\Sender Инфрмация об отправителе
     */
    protected $sender;
    /**
     * @var Recipient Информация о получателе
     */
    protected $recipient;
    /**
     * @var Item[] Массив с вложениями
     */
    protected $items;
    /**
     * @var \Apiship\Entity\Request\Part\Place[] Массив с местами для перевозки
     */
    protected $places;
    /**
     * @var \Apiship\Entity\Request\Part\ExtraParam[] Массив доп параметрами
     */
    protected $extraParams;

    /**
     * @return OrderData
     * @throws RequiredParameterException
     */
    public function getOrder()
    {
        if (!$this->order) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::order" is required.
                ');
        }

        return $this->order;
    }

    /**
     * @param OrderData $order
     *
     * @return OrderRequest
     */
    public function setOrder(OrderData $order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return \Apiship\Entity\Request\Part\Cost
     * @throws RequiredParameterException
     */
    public function getCost()
    {
        if (!$this->cost) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::cost" is required.
                ');
        }

        return $this->cost;
    }

    /**
     * @param \Apiship\Entity\Request\Part\Cost $cost
     *
     * @return OrderRequest
     */
    public function setCost(Cost $cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return \Apiship\Entity\Request\Part\Sender
     * @throws RequiredParameterException
     */
    public function getSender()
    {
        if (!$this->sender) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::sender" is required.
                ');
        }

        return $this->sender;
    }

    /**
     * @param \Apiship\Entity\Request\Part\Sender $sender
     *
     * @return OrderRequest
     */
    public function setSender(Sender $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return Recipient
     * @throws RequiredParameterException
     */
    public function getRecipient()
    {
        if (!$this->recipient) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::recipient" is required.
                ');
        }

        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     *
     * @return OrderRequest
     */
    public function setRecipient(Recipient $recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     *
     * @return OrderRequest
     */
    public function setItems(array $items)
    {
        foreach ($items as $item) {
            $this->addItem($item);
        }

        return $this;
    }

    /**
     * @param Item $item
     *
     * @return OrderRequest
     */
    public function addItem(Item $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @return \Apiship\Entity\Request\Part\Place[]
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @param \Apiship\Entity\Request\Part\Place[] $places
     *
     * @return OrderRequest
     */
    public function setPlaces(array $places)
    {
        foreach ($places as $place) {
            $this->addPlace($place);
        }

        return $this;
    }

    /**
     * @param \Apiship\Entity\Request\Part\Place $place
     *
     * @return OrderRequest
     */
    public function addPlace(Place $place)
    {
        $this->places[] = $place;

        return $this;
    }

    /**
     * @return ExtraParam[]
     */
    public function getExtraParams()
    {
        return $this->extraParams;
    }

    /**
     * @param \Apiship\Entity\Request\Part\ExtraParam[] $extraParams
     *
     * @return OrderRequest
     */
    public function setExtraParams(array $extraParams)
    {
        foreach ($extraParams as $extraParam) {
            $this->addExtraParam($extraParam);
        }

        return $this;
    }

    /**
     * @param ExtraParam $extraParam
     *
     * @return OrderRequest
     */
    public function addExtraParam(ExtraParam $extraParam)
    {
        $this->extraParams[] = $extraParam;

        return $this;
    }
}