<?php

namespace Apiship\Entity\Request\Part;

use Apiship\Entity\AbstractRequestPart;
use Apiship\Exception\RequiredParameterException;

class Cost extends AbstractRequestPart
{
	/**
	 * @var float Сумма страховки (в рублях)
	 */
	protected $insuranceCost;
    /**
     * @var float Оценочная стоимость / сумма страховки (в рублях)
     */
    protected $assessedCost;
    /**
     * @var float Стоимость доставки с учетом НДС (в рублях)
     */
    protected $deliveryCost;
	/**
	 * @var float Процентная ставка НДС
	 */
    protected $deliveryCostVat;
    /**
     * @var float Сумма наложенного платежа с учетом НДС (в рублях)
     */
    protected $codCost;
    /**
     * @var bool Флаг для указания стороны которая платит за услуги доставки (false-отправитель, true-получатель)
     */
    protected $isDeliveryPayedByRecipient;
	
	/**
	 * @return float
	 */
	public function getInsuranceCost()
	{
		return $this->insuranceCost;
	}
	
	/**
	 * @param float $insuranceCost
	 *
	 * @return Cost
	 */
	public function setInsuranceCost($insuranceCost)
	{
		$this->insuranceCost = $insuranceCost;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getDeliveryCostVat()
	{
		return $this->deliveryCostVat;
	}
	
	/**
	 * @param float $deliveryCostVat
	 *
	 * @return Cost
	 */
	public function setDeliveryCostVat($deliveryCostVat)
	{
		$this->deliveryCostVat = $deliveryCostVat;
		return $this;
	}

    /**
     * @return float
     * @throws RequiredParameterException
     */
    public function getAssessedCost()
    {
        if (!$this->assessedCost) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::assessedCost" is required.
                ');
        }

        return $this->assessedCost;
    }

    /**
     * @param float $assessedCost
     *
     * @return Cost
     */
    public function setAssessedCost($assessedCost)
    {
        $this->assessedCost = $assessedCost;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

    /**
     * @param float $deliveryCost
     *
     * @return Cost
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;
        return $this;
    }

    /**
     * @return float
     * @throws RequiredParameterException
     */
    public function getCodCost()
    {
        if (!$this->codCost) {
            throw new RequiredParameterException(
                'Property "' . get_class($this) . '::codCost" is required.
                ');
        }

        return $this->codCost;
    }

    /**
     * @param float $codCost
     *
     * @return Cost
     */
    public function setCodCost($codCost)
    {
        $this->codCost = $codCost;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsDeliveryPayedByRecipient()
    {
        return $this->isDeliveryPayedByRecipient;
    }

    /**
     * @param boolean $isDeliveryPayedByRecipient
     *
     * @return Cost
     */
    public function setIsDeliveryPayedByRecipient($isDeliveryPayedByRecipient)
    {
        $this->isDeliveryPayedByRecipient = $isDeliveryPayedByRecipient;
        return $this;
    }
}