<?php

namespace Apiship\Entity\Request;
use Apiship\Entity\AbstractRequest;

class LabelsRequest extends AbstractRequest
{
	/**
	 * формат наклеек по умолчанию
	 * @const string
	 */
	const LABELS_FORMAT__PDF = 'pdf';
	/**
	 * @var int[]
	 */
	protected $orderIds;
	/**
	 * @var string
	 */
	protected $format;

	/**
	 * @return \int[]
	 */
	public function getOrderIds()
	{
		return $this->orderIds;
	}

	/**
	 * @param \int[] $orderIds
	 * @return LabelsRequest
	 */
	public function setOrderIds($orderIds)
	{
		$this->orderIds = $orderIds;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * @param string $format
	 * @return LabelsRequest
	 */
	public function setFormat($format)
	{
		$this->format = $format;
		return $this;
	}
}