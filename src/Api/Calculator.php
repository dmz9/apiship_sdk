<?php

namespace Apiship\Api;

use Apiship\Entity\Request\CalculatorRequest;
use Apiship\Entity\Response\CalculatorResponse;

use Apiship\Entity\Response\Part\CalculatorToDoorResult;
use Apiship\Entity\Response\Part\CalculatorToPointResult;

use Apiship\Entity\Response\Part\TariffToDoor;
use Apiship\Entity\Response\Part\TariffToPoint;

class Calculator extends AbstractApi
{
	/**
	 * Расчитывает стоимость доставки
	 *
	 * @param CalculatorRequest $request
	 *
	 * @return CalculatorResponse
	 */
	public function calculate(CalculatorRequest $request)
	{
		$resultJson = $this->adapter->post(
			'calculator',
			[],
			$request->asJson()
		);
		$result     = json_decode($resultJson);
		
		$response = new CalculatorResponse();
		$response->setOriginJson($resultJson);
		
		if (!empty($result->deliveryToDoor)) {
			foreach ($result->deliveryToDoor as $item) {
				$calculatorResult = new CalculatorToDoorResult();
				if (isset($item->providerKey)) {
					$calculatorResult->setProviderKey($item->providerKey);
				}
				if (isset($item->tariffs)) {
					foreach ($item->tariffs as $itemTariff) {
						$tariff = new TariffToDoor();
						foreach ($itemTariff as $key => $value) {
							$tariff->$key = $value;
						}
						$calculatorResult->addTariff($tariff);
					}
				}
				$response->addDeliveryToDoor($calculatorResult);
			}
		}
		if (!empty($result->deliveryToPoint)) {
			foreach ($result->deliveryToPoint as $item) {
				$calculatorResult = new CalculatorToPointResult();
				if (isset($item->providerKey)) {
					$calculatorResult->setProviderKey($item->providerKey);
				}
				if (isset($item->tariffs)) {
					foreach ($item->tariffs as $itemTariff) {
						$tariff = new TariffToPoint();
						foreach ($itemTariff as $key => $value) {
							$tariff->$key = $value;
						}
						$calculatorResult->addTariff($tariff);
					}
				}
				$response->addDeliveryToPoint($calculatorResult);
			}
		}
		
		return $response;
	}
}
