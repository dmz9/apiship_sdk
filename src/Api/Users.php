<?php

namespace Apiship\Api;

use Apiship\Adapter\GuzzleAdapter;
use Apiship\Entity\Response\AutoregistrationResponse;

class Users extends AbstractApi
{
	/**
	 * получение токена.
	 *
	 * @param $login    string
	 * @param $password string
	 *
	 * @return string
	 */
	public function getToken($login, $password)
	{
		$login = trim((string)$login);
		if (empty($login)) {
			throw new \InvalidArgumentException("Логин не может быть пустым");
		} else if (strlen($login) < 8 || strlen($login) > 30) {
			throw new \InvalidArgumentException("Длина логина может быть от 8 до 30 символов");
		}
		
		$password = trim((string)$password);
		if (empty($password)) {
			throw new \InvalidArgumentException("Пароль не может быть пустым");
		} else if (strlen($password) < 8 || strlen($password) > 30) {
			throw new \InvalidArgumentException("Длина пароля может быть от 8 до 30 символов");
		}
		
		$response = $this->adapter->post(
			'login',
			[],
			[
				'login' => $login,
				'password' => $password
			]
		);
		
		return json_decode($response, true)['accessToken'];
	}
	
	/**
	 * @param       $login    string
	 * @param       $password string|null
	 */
	public function autoRegistration($login, $password = null)
	{
		$login    = trim((string)$login);
		$password = trim((string)$password);
		
		if (empty($login)) {
			throw new \InvalidArgumentException("Логин не может быть пустым");
		} else if (strlen($login) < 8 || strlen($login) > 30) {
			throw new \InvalidArgumentException("Длина логина может быть от 8 до 30 символов");
		}
		
		$request = ['login' => $login];
		
		if (!empty($password)) {
			if (strlen($password) < 8 || strlen($password) > 30) {
				throw new \InvalidArgumentException("Длина пароля может быть от 8 до 30 символов");
			}
			$request['password'] = $password;
		}
		
		$resultJson = $this->adapter->post(
			'autoregistration',
			[],
			$request
		);
		$result     = json_decode($resultJson);
		
		$response = new AutoregistrationResponse();
		$response->setOriginJson($resultJson);
		
		if (!empty($result->login)) {
			$response->setLogin($result->login);
		}
		if (!empty($result->password)) {
			$response->setPassword($result->password);
		}
		
		return $response;
	}
}